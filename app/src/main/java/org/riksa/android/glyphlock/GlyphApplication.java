/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import org.riksa.android.glyphlock.dagger.AndroidModule;
import org.riksa.android.glyphlock.dagger.ApplicationComponent;
import org.riksa.android.glyphlock.dagger.DaggerApplicationComponent;

import timber.log.Timber;

/**
 * Created by riksa on 09/08/15.
 */
public class GlyphApplication extends Application {
    private ApplicationComponent component;
    private boolean locked = false;
    private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        refWatcher = LeakCanary.install(this);

        component = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }

    public static RefWatcher getRefWatcher(Context context) {
        GlyphApplication application = (GlyphApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    public ApplicationComponent getApplicationComponent() {
        return component;
    }

    public void setLocked(boolean locked) {
        Timber.d("setLock %b", locked);
        this.locked = locked;
    }

    public boolean isLocked() {
        return locked;
    }
}
