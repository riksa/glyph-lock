/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.dagger;

import org.riksa.android.glyphlock.ui.DefineUnlockSequenceActivity;
import org.riksa.android.glyphlock.ui.DefineUnlockSequenceActivityFragment;
import org.riksa.android.glyphlock.ui.GlyphListFragment;
import org.riksa.android.glyphlock.ui.GlyphLockActivity;
import org.riksa.android.glyphlock.ui.GlyphPreferenceFragment;
import org.riksa.android.glyphlock.ui.GlyphSequenceFragment;
import org.riksa.android.glyphlock.ui.GlyphView;
import org.riksa.android.glyphlock.ui.MainActivity;
import org.riksa.android.glyphlock.ui.SequenceListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by riksa on 09/08/15.
 */
@Singleton
@Component(modules = AndroidModule.class)
public interface ApplicationComponent {
    void inject(MainActivity activity);

    void inject(GlyphLockActivity activity);

    void inject(GlyphListFragment fragment);

    void inject(SequenceListFragment sequenceListFragment);

    void inject(GlyphPreferenceFragment glyphPreferenceFragment);

    void inject(GlyphSequenceFragment glyphSequenceFragment);

    void inject(GlyphView glyphView);

    void inject(DefineUnlockSequenceActivityFragment defineUnlockSequenceActivityFragment);

    void inject(DefineUnlockSequenceActivity defineUnlockSequenceActivity);
}
