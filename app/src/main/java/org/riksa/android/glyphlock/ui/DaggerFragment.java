/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.ui;


import android.support.v4.app.Fragment;

import com.squareup.leakcanary.RefWatcher;

import org.riksa.android.glyphlock.GlyphApplication;
import org.riksa.android.glyphlock.dagger.ApplicationComponent;

/**
 * Created by riksa on 10/08/15.
 */
public abstract class DaggerFragment extends Fragment {
    protected ApplicationComponent getApplicationComponent() {
        return ((GlyphApplication) getActivity().getApplicationContext()).getApplicationComponent();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RefWatcher refWatcher = GlyphApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

}
