/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.model;

import com.google.common.base.Optional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import timber.log.Timber;

/**
 * Created by riksa on 14/08/15.
 */
public class SequenceList {
    /**
     * From JSON
     */
    private List<List<String>> sequences;

    /**
     * Resolved glyphs from glyphs.json
     */
    private List<Sequence> sequenceList;

    @Deprecated
    public List<List<String>> getSequences() {
        return sequences;
    }

    @Override
    public String toString() {
        return "SequenceList{" +
                "sequences=" + sequences +
                '}';
    }

    public void resolveGlyphs(GlyphResolver glyphResolver) {
        final List<Glyph> glyphs = glyphResolver.getGlyphs();

        // add all single glyphs as sequence of 1
        Timber.d("adding individual glyphs as sequences");
        for (Glyph glyph : glyphs) {
            for (String name : glyph.getNames()) {
                // some glyphs have several names, so they will be added multiple times
                sequences.add(0, Arrays.asList(name));
            }
        }


        sequenceList = new ArrayList<>(sequences.size());
        for (List<String> sequence : sequences) {
            Sequence s = new Sequence();
            for (String name : sequence) {
                final Optional<Glyph> glyphByName = glyphResolver.getGlyphByName(name);
                if (glyphByName.isPresent()) {
                    Glyph glyph = glyphByName.get();
                    glyph.setPrimaryName(name);
                    s.add(name, glyph);
                } else {
                    Timber.e("Cannot find glyph %s in sequence %s", name, sequence);
                }

            }
            sequenceList.add(s);
        }

    }

    public Sequence getSequence(int i) {
        return sequenceList.get(i);
    }

    public int size() {
        return sequenceList.size();
    }

    public List<Sequence> getSequenceList() {
        return sequenceList;
    }
}
