/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.model;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

/**
 * Created by riksa on 09/08/15.
 */
public class GlyphResolverImpl implements GlyphResolver {
    private final List<Glyph> glyphs = new ArrayList<>();

    public GlyphResolverImpl(List<Glyph> glyphList) {
        glyphs.addAll(glyphList);
    }

    @Override
    public Optional<Glyph> getGlyphByName(String name) {
        return Iterables.tryFind(glyphs, createNamePredicate(name));
    }

    @Override
    public Optional<Glyph> detectGlyph(List<Integer> inputSequence) {
        if (inputSequence == null || inputSequence.size() < 2) {
            return Optional.absent();
        }

        for (Glyph glyph : glyphs) {
            if( glyph.matches(inputSequence)) {
                return Optional.of(glyph);
            }
        }

        return Optional.absent();
    }

    @Override
    public List<Glyph> getGlyphs() {
        return glyphs;
    }

    private Predicate<? super Glyph> createNamePredicate(final String name) {
        return new Predicate<Glyph>() {
            @Override
            public boolean apply(Glyph input) {
                return name != null && input.hasName(name);
            }
        };
    }
}
