/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.riksa.android.glyphlock.ui.GlyphView;
import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.Sequence;
import org.riksa.android.glyphlock.model.SequenceList;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by riksa on 14/08/15.
 */
public class SequenceAdapter extends RecyclerView.Adapter<SequenceAdapter.ViewHolder> {

    private final SequenceList sequenceList;

    public SequenceAdapter(SequenceList sequenceList) {
        this.sequenceList = sequenceList;
        Timber.d("sequenceList %s", sequenceList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.sequence_row_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final Sequence s = sequenceList.getSequence(i);

        ButterKnife.apply(viewHolder.sequenceViews, new ButterKnife.Action<ViewGroup>() {
            @Override
            public void apply(ViewGroup view, int index) {
                view.setVisibility(View.GONE);
            }
        });

        final List<Glyph> glyphs = s.getGlyphs();
        final List<String> names = s.getSequence();

        for (int c = 0; c < glyphs.size(); c++) {
            final View view = viewHolder.sequenceViews.get(c);

            GlyphView glyphView = ButterKnife.findById(view, R.id.glyph_view);
            glyphView.setShowGlyph(true);
            glyphView.setGlyph(glyphs.get(c));

            ImageView glyphViewBackground = ButterKnife.findById(view, R.id.glyph_background);
            glyphViewBackground.setImageLevel(0);

            TextView textView = ButterKnife.findById(view, R.id.glyph_name);
            textView.setText(names.get(c));

            view.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return sequenceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind({R.id.glyph_1, R.id.glyph_2, R.id.glyph_3, R.id.glyph_4, R.id.glyph_5})
        List<ViewGroup> sequenceViews;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
