/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.dagger;

import android.app.Application;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.gson.Gson;

import org.riksa.android.glyphlock.BuildConfig;
import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.factory.GlyphFactory;
import org.riksa.android.glyphlock.factory.GlyphFactoryImpl;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.GlyphList;
import org.riksa.android.glyphlock.model.GlyphResolver;
import org.riksa.android.glyphlock.model.GlyphResolverImpl;
import org.riksa.android.glyphlock.model.SequenceList;
import org.riksa.android.glyphlock.model.SequenceProvider;
import org.riksa.android.glyphlock.model.SequenceProviderImpl;
import org.riksa.android.glyphlock.ui.GlyphPreferenceFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timber.log.Timber;

/**
 * Created by riksa on 09/08/15.
 */
@Module
public class AndroidModule {
    private final Application application;

    public AndroidModule(Application application) {
        this.application = application;
    }

    @Provides
    Gson provideGson() {
        return new Gson();

    }

    @Singleton
    @Provides
    GlyphList provideGlyphList(Gson gson) {
        try {
            final InputStream inputStream = application.getResources().openRawResource(R.raw.glyphs);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            final GlyphList glyphList = gson.fromJson(bufferedReader, GlyphList.class);
            bufferedReader.close();
            return glyphList;
        } catch (IOException e) {
            Timber.e(e, e.getMessage());
        }
        return null;
    }

    @Singleton
    @Provides
    SequenceList provideSequenceList(Gson gson, GlyphResolver glyphResolver) {
        try {
            final InputStream inputStream = application.getResources().openRawResource(R.raw.sequences);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            final SequenceList sequenceList = gson.fromJson(bufferedReader, SequenceList.class);
            sequenceList.resolveGlyphs(glyphResolver);
            bufferedReader.close();
            return sequenceList;
        } catch (IOException e) {
            Timber.e(e, e.getMessage());
        }
        return null;
    }

    @Provides
    SequenceProvider provideSequenceProvider(
            final SequenceList sequenceList,
            GlyphFactory glyphFactory,
            final GlyphResolver glyphResolver,
            final @Named("sequenceLength") int length) {
        if (GlyphPreferenceFragment.isCustomSequenceEnabled(application)) {
            final Optional<List<Glyph>> unlockSequenceOptional = GlyphPreferenceFragment.getUnlockSequence(application, glyphFactory, glyphResolver);
            if (unlockSequenceOptional.isPresent()) {
                return new SequenceProvider() {
                    final List<Glyph> unlockSequence = unlockSequenceOptional.get();

                    @Override
                    public List<Glyph> getSequence() {
                        return unlockSequence;
                    }
                };
            }
        }

        return new SequenceProviderImpl(sequenceList, length);
    }

    @Provides
    GlyphResolver provideGlyphResolver(GlyphList glyphList) {
        return new GlyphResolverImpl(glyphList.getGlyphs());
    }

    @Provides
    @Named("versionCode")
    int provideVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    @Provides
    @Named("sequenceLength")
    int provideSequenceLength() {
        return GlyphPreferenceFragment.getSelectedSequenceSize(application);
    }

    @Provides
    @Named("versionName")
    String provideVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    @Provides
    @Named("versionString")
    String provideVersionString(@Named("versionCode") int versionCode, @Named("versionName") String versionName) {
        return String.format("%s (%d)", versionName, versionCode);
    }

    @Provides
    GlyphFactory glyphFactory() {
        return new GlyphFactoryImpl();
    }

}
