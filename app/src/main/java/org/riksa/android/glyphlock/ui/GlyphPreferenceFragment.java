/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.ui;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;

import com.github.machinarius.preferencefragment.PreferenceFragment;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.squareup.leakcanary.RefWatcher;

import org.riksa.android.glyphlock.GlyphApplication;
import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.dagger.ApplicationComponent;
import org.riksa.android.glyphlock.factory.GlyphFactory;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.GlyphResolver;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

public class GlyphPreferenceFragment extends PreferenceFragment {

    private static final String PREFERENCE_VERSION = "preference_version";
    private static final String PREFERENCE_SHOW_GLYPH = "preference_show_glyph";
    private static final String PREFERENCE_SHOW_SEQUENCE = "preference_show_sequence";
    private static final String PREFERENCE_SHOW_GLYPH_TITLE = "preference_show_glyph_title";
    private static final String PREFERENCE_ENABLED_BACK_UNLOCK = "preference_enable_back_unlock";
    private static final String PREFERENCE_ENABLED_BACK_UNLOCK_PASSWORD = "preference_enable_back_unlock_password_enable";
    private static final String PREFERENCE_BACK_UNLOCK_PASSWORD = "preference_enable_back_unlock_password";
    private static final String PREFERENCE_RANDOM_SEQUENCE = "preference_random_sequence";
    private static final String PREFERENCE_ENABLE_CUSTOM_SEQUENCE = "preference_enable_custom_sequence";
    private static final String PREFERENCE_UNLOCK_SEQUENCE = "preference_unlock_sequence";
    private static final int REQUEST_CODE_SEQUENCE = 1;
    private static final int REQUEST_CODE_VERIFY = 2;
    private static final String SEQUENCE_SEPARATOR = ":";
    private static final String INDEX_SEPARATOR = ",";

    @Inject
    protected GlyphResolver glyphResolver;

    @Inject
    protected GlyphFactory glyphFactory;

    @Inject
    @Named("versionString")
    protected String versionString;

    public GlyphPreferenceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);

        addPreferencesFromResource(R.xml.preferences);

        Preference preferenceVersion = findPreference(PREFERENCE_VERSION);
        preferenceVersion.setSummary(versionString);
    }

    @Override
    public void onResume() {
        super.onResume();

        final Preference preferenceRandomSequence = findPreference(PREFERENCE_RANDOM_SEQUENCE);
        preferenceRandomSequence.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startSequenceDefinitionActivity();
                return true;
            }
        });

        if (getUnlockSequence(getActivity(), glyphFactory, glyphResolver).isPresent()) {
            preferenceRandomSequence.setSummary(R.string.preference_random_sequence_summary_defined);
        } else {
            preferenceRandomSequence.setSummary(R.string.preference_random_sequence_summary_not_defined);
        }

        final CheckBoxPreference preferenceEnableCustomSequence = (CheckBoxPreference) findPreference(PREFERENCE_ENABLE_CUSTOM_SEQUENCE);
        preferenceEnableCustomSequence.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (!(Boolean) newValue) {
                    // random enabled, that's fine always
                    return true;
                }

                // trying to enable custom sequence, do some sanity checking
                final Optional<List<Glyph>> unlockSequence = getUnlockSequence(getActivity(), glyphFactory, glyphResolver);
                if (unlockSequence.isPresent()) {
                    final String sequence = sequenceAsString(unlockSequence.get());

                    Snackbar.make(getView(), getString(R.string.preference_random_sequence_verification_title, sequence), Snackbar.LENGTH_LONG)
                            .setAction(R.string.preference_random_sequence_verification_action, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startSequenceVerificationActivity();
                                }
                            })
                            .show();

                } else {
                    Snackbar.make(getView(), R.string.preference_random_sequence_summary_not_defined_toast, Snackbar.LENGTH_LONG)
                            .setAction(R.string.preference_random_sequence_title_action, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startSequenceDefinitionActivity();
                                }
                            })
                            .show();
                }
                return false;
            }
        });

    }

    @Override
    public void onPause() {
        final Preference preferenceRandomSequence = findPreference(PREFERENCE_RANDOM_SEQUENCE);
        preferenceRandomSequence.setOnPreferenceClickListener(null);

        final Preference preferenceEnableCustomSequence = findPreference(PREFERENCE_ENABLE_CUSTOM_SEQUENCE);
        preferenceEnableCustomSequence.setOnPreferenceChangeListener(null);

        super.onPause();
    }

    @Override
    public void onDestroy() {
        RefWatcher refWatcher = GlyphApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);

        super.onDestroy();
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((GlyphApplication) getActivity().getApplicationContext()).getApplicationComponent();
    }

    public static int getSelectedSequenceSize(Context context) {
        final int defaultValue = context.getResources().getInteger(R.integer.preference_sequence_level);
        try {
            final String preference_sequence_level = getSharedPreferences(context).getString("preference_sequence_level",
                    String.valueOf(defaultValue));
            return Integer.parseInt(preference_sequence_level);
        } catch (NumberFormatException e) {
            Timber.e(e, e.getMessage());
            return defaultValue;
        }
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Deprecated
    public static boolean isRandomSequenceEnabled(Context context) {
        return !isCustomSequenceEnabled(context);
    }

    public static boolean isCustomSequenceEnabled(Context context) {
        return getSharedPreferences(context).getBoolean(PREFERENCE_ENABLE_CUSTOM_SEQUENCE, context.getResources().getBoolean(R.bool.preference_enable_custom_sequence));
    }

    public static boolean isShowGlyph(Context context) {
        return getSharedPreferences(context).getBoolean(PREFERENCE_SHOW_GLYPH, context.getResources().getBoolean(R.bool.preference_show_glyph));
    }

    public static boolean isShowSequence(Context context) {
        return getSharedPreferences(context).getBoolean(PREFERENCE_SHOW_SEQUENCE, context.getResources().getBoolean(R.bool.preference_show_sequence));
    }

    public static boolean isShowGlyphTitle(Context context) {
        return getSharedPreferences(context).getBoolean(PREFERENCE_SHOW_GLYPH_TITLE, context.getResources().getBoolean(R.bool.preference_show_glyph_title));
    }

    public static boolean isBackUnlockEnabled(Context context) {
        return getSharedPreferences(context).getBoolean(PREFERENCE_ENABLED_BACK_UNLOCK, context.getResources().getBoolean(R.bool.preference_enable_back_unlock));
    }

    public static boolean isOverridePasswordRequired(Context context) {
        return getSharedPreferences(context).getBoolean(PREFERENCE_ENABLED_BACK_UNLOCK_PASSWORD, context.getResources().getBoolean(R.bool.preference_enable_back_unlock_password_enable));
    }

    public static boolean overridePasswordMatches(Context context, CharSequence password) {
        final String configuredPassword = getSharedPreferences(context).getString(PREFERENCE_BACK_UNLOCK_PASSWORD, null);
        if (TextUtils.isEmpty(password) || TextUtils.isEmpty(configuredPassword)) {
            return TextUtils.isEmpty(password) && TextUtils.isEmpty(configuredPassword);
        }

        // neither is null, nor empty
        return configuredPassword.equals(password.toString());
    }

    private void startSequenceDefinitionActivity() {
        Intent intent = new Intent(getActivity(), DefineUnlockSequenceActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SEQUENCE);
    }

    private void startSequenceVerificationActivity() {
        Optional<List<Glyph>> unlockSequence = getUnlockSequence(getActivity(), glyphFactory, glyphResolver);
        if( unlockSequence.isPresent() ) {
            Intent intent = new Intent(getActivity(), DefineUnlockSequenceActivity.class);
            intent.putExtra(DefineUnlockSequenceActivity.EXTRA_MODE, DefineUnlockSequenceActivity.MODE_VERIFY);
            intent.putExtra(DefineUnlockSequenceActivity.EXTRA_SEQUENCE, (Serializable) unlockSequence.get());
            startActivityForResult(intent, REQUEST_CODE_VERIFY);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SEQUENCE:
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null && data.hasExtra(DefineUnlockSequenceActivity.EXTRA_SEQUENCE)) {
                        List<Glyph> sequence = (List<Glyph>) data.getSerializableExtra(DefineUnlockSequenceActivity.EXTRA_SEQUENCE);
                        Timber.d("Got sequence %s", sequence);
                        setUnlockSequence(getActivity(), sequence);
                    }
                } else {
                    Timber.d("sequence canceled");
                }
                break;
            case REQUEST_CODE_VERIFY:
                if (resultCode == Activity.RESULT_OK) {
                    final CheckBoxPreference preferenceCustomSequence = (CheckBoxPreference) findPreference(PREFERENCE_ENABLE_CUSTOM_SEQUENCE);
                    preferenceCustomSequence.setChecked(true);
                    Snackbar.make(getView(), R.string.preference_custom_sequence_enabled, Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    Snackbar.make(getView(), R.string.preference_custom_sequence_not_enabled, Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private static void setUnlockSequence(Context context, List<Glyph> sequence) {
        final String join = sequenceAsString(sequence);

        getSharedPreferences(context).edit()
                .putString(PREFERENCE_UNLOCK_SEQUENCE, join)
                .commit();
    }

    @NonNull
    private static String sequenceAsString(List<Glyph> sequence) {
        final Joiner indexJoiner = Joiner.on(INDEX_SEPARATOR);

        String join = FluentIterable.from(sequence)
                .transform(new Function<Glyph, String>() {
                    @Override
                    public String apply(Glyph input) {
                        return indexJoiner.join(input.getIndices());
                    }
                })
                .join(Joiner.on(SEQUENCE_SEPARATOR));

        Timber.d("Unlock sequence is %s", join);

        return join;
    }

    public static Optional<List<Glyph>> getUnlockSequence(Context context, final GlyphFactory glyphFactory, final GlyphResolver glyphResolver) {
        Optional<List<String>> unlockSequenceStrings = getUnlockSequenceStrings(context);
        if (!unlockSequenceStrings.isPresent()) {
            return Optional.absent();
        }

        final Splitter indexSplitter = Splitter.on(INDEX_SEPARATOR);
        List<Glyph> glyphs = FluentIterable.from(unlockSequenceStrings.get())
                .transform(new Function<String, List<Integer>>() {
                    @Override
                    public List<Integer> apply(String input) {
                        Iterable<String> split = indexSplitter.split(input);
                        Timber.d("indices %s -> %s", input, split);
                        return FluentIterable.from(split)
                                .transform(new Function<String, Integer>() {
                                    @Override
                                    public Integer apply(String input) {
                                        try {
                                            return Integer.parseInt(input);
                                        } catch (NumberFormatException e) {
                                            Timber.e(e, e.getMessage());
                                            return 0;
                                        }
                                    }
                                }).toList();
                    }
                })
                .transform(new Function<List<Integer>, Glyph>() {
                    @Override
                    public Glyph apply(List<Integer> input) {
                        final Optional<Glyph> glyphOptional = glyphResolver.detectGlyph(input);
                        if (glyphOptional.isPresent()) {
                            return glyphOptional.get();
                        } else {
                            // custom glyph
                            return glyphFactory.fromSequence("", input);
                        }
                    }
                })
                .toList();
        ;

        if (glyphs.contains(null)) {
            Timber.e("something went wrong parsing sequence %s to glyphs %s", unlockSequenceStrings, glyphs);
            return Optional.absent();
        }

        Timber.d("Unlock sequence is %s", glyphs);
        return Optional.of(glyphs);

    }


    // Cannot use list of names anymore because we have custom glyphs now
    @Deprecated
    private static Optional<List<String>> getUnlockSequenceStrings(Context context) {
        final String sequence = getSharedPreferences(context).getString(PREFERENCE_UNLOCK_SEQUENCE, null);

        if (sequence == null) {
            return Optional.absent();
        }

        return Optional.of(Splitter.on(SEQUENCE_SEPARATOR).splitToList(sequence));
    }
}
