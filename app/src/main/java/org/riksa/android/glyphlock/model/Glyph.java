/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.model;

import android.graphics.PointF;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

/**
 * Created by riksa on 09/08/15.
 */
public class Glyph implements Serializable {
    public static final int POINTS = 11;
    private List<String> names = Collections.emptyList();
    private List<Integer> indices = Collections.emptyList();

    private static final float INNER_DISTANCE = 0.33f / 2;
    private static final float OUTER_DISTANCE = 2 * INNER_DISTANCE;

    public static final Glyph EMPTY = new Glyph();
    private String primaryName;

    public Glyph() {
    }

    public Glyph(List<String> names, List<Integer> indices) {
        this.names = names;
        this.indices = indices;
    }

    public static PointF getPoint(int index, int glyphSize) {
        final PointF point = new PointF(0, 0);

        if (index < 0 || index >= 10) {
            // 10
            // center (and default);
        } else if (index < 6) {
            // 0, 1, 2, 3, 4, 5
            // outer ring
            // angle 0 is top, flipped y and x axis
            final float dy = (float) (OUTER_DISTANCE * glyphSize * Math.cos(Math.PI / 3f * index));
            final float dx = (float) (OUTER_DISTANCE * glyphSize * Math.sin(Math.PI / 3f * index));

            point.offset(dx, dy);
        } else if (index < 8) {
            // 6, 7
            // inner ring, right side
            // angle 0 is top, flipped y and x axis
            final float dy = (float) (INNER_DISTANCE * glyphSize * Math.cos(Math.PI / 3f * (index - 5)));
            final float dx = (float) (INNER_DISTANCE * glyphSize * Math.sin(Math.PI / 3f * (index - 5)));

            point.offset(dx, dy);
        } else {
            // 8, 9
            // inner ring, left side
            // angle 0 is top, flipped y and x axis
            final float dy = (float) (INNER_DISTANCE * glyphSize * Math.cos(Math.PI / 3f * (index - 4)));
            final float dx = (float) (INNER_DISTANCE * glyphSize * Math.sin(Math.PI / 3f * (index - 4)));

            point.offset(dx, dy);
        }


        return point;
    }

    public int size() {
        return indices.size();
    }

    public int getIndex(int c) {
        if (c >= 0 && c < indices.size()) {
            return indices.get(c);
        }

        Timber.e(new Throwable(), "No such index %d, %s", c, this);
        return 0;
    }

    @Override
    public String toString() {
        return "Glyph{" +
                "names=" + names +
                ", indices=" + indices +
                '}';
    }

    public boolean hasName(String name) {
        return Iterables.contains(names, name);
    }

    public String getTitle() {
        return Joiner.on(", ").join(names);
    }

    public static Optional<Integer> getPointForXY(float x, float y, int glyphSize, int detectionRadius) {
        for (int c = 0; c < POINTS; c++) {
            final PointF point = getPoint(c, glyphSize);
            point.offset(-x, -y);
            if (Math.abs(point.x) < detectionRadius && Math.abs(point.y) < detectionRadius) {
                Timber.d("hit point %d", c);
                return Optional.of(c);
            }
        }
        return Optional.absent();
    }

    public static List<Integer> getEdges(List<Integer> inputSequence) {
        if (inputSequence == null || inputSequence.size() < 2) {
            return Collections.emptyList();
        }

        final List<Integer> edges = new ArrayList<>(inputSequence.size() - 1);

        Integer start = inputSequence.get(0);
        for (int i = 1; i < inputSequence.size(); i++) {
            Integer stop = inputSequence.get(i);
            edges.add(getEdge(start, stop));
            start = stop;
        }

        return edges;
    }

    /**
     * returns id for an edge between 2 points. A-B == B-A, A-A = -1
     *
     * @param start
     * @param stop
     * @return
     */
    private static Integer getEdge(Integer start, Integer stop) {
        if (start == null || stop == null || start == stop) {
            return -1;
        }

        return Math.min(start, stop) * 256 + Math.max(start, stop);
    }

    public List<Integer> getIndices() {
        return indices;
    }

    public List<String> getNames() {
        return names;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public boolean matches(List<Integer> inputSequence) {
        Set<Integer> inputEdges = new HashSet<>(Glyph.getEdges(inputSequence));
        Set<Integer> glyphEdges = new HashSet<>(Glyph.getEdges(this.getIndices()));
        Timber.d("inputSequence %s", inputSequence);
        Timber.d("inputEdges %s", inputEdges);
        Timber.d("glyphEdges %s, %s", glyphEdges, this);
        return glyphEdges.equals(inputEdges);
    }
}
