/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.google.common.base.Optional;

import org.riksa.android.glyphlock.GlyphApplication;
import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.dagger.ApplicationComponent;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.GlyphResolver;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * TODO: document your custom view class.
 */
public class GlyphView extends View {
    private int lineColor = Color.BLUE;
    private int inputColor = Color.BLUE;

    private float lineWidth = 4;
    private String glyphName = "clear";
    private Glyph glyph;
    private Paint paintGlyph;
    private Paint paintInput;
    private final double aspectRatio = 2 / Math.sqrt(3);
    private boolean showGlyph = true;
    private boolean inputMode = false;

    @Inject
    GlyphResolver glyphResolver;

    final List<Integer> glyphSequence = new ArrayList<>(10);
    final List<Integer> inputSequence = new ArrayList<>(10);

    private int contentWidth;
    private int contentHeight;
    private int glyphSize;
    private float pointRadius = 10;

    private float pointerX;
    private float pointerY;
    private boolean drawing = false;
    private float pointerRadius = 10;

    private InputListener inputListener;

    public GlyphView(Context context) {
        super(context);
        injectThis();
        init(null, 0);
    }

    private void injectThis() {
        if (!isInEditMode()) {
            getApplicationComponent().inject(this);
        }
    }

    public GlyphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        injectThis();
        init(attrs, 0);
    }

    public GlyphView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        injectThis();
        init(attrs, defStyle);
    }

    private ApplicationComponent getApplicationComponent() {
        return ((GlyphApplication) getContext().getApplicationContext()).getApplicationComponent();
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.GlyphView, defStyle, 0);

        glyphName = a.getString(
                R.styleable.GlyphView_glyphName);

        if (isInEditMode()) {
            setGlyph(Glyph.EMPTY);
        } else {
            setGlyph(glyphResolver.getGlyphByName(glyphName).or(Glyph.EMPTY));
        }

        lineColor = a.getColor(
                R.styleable.GlyphView_lineColor,
                getResources().getColor(R.color.defaultLineColor));

        inputColor = a.getColor(
                R.styleable.GlyphView_inputColor,
                getResources().getColor(R.color.defaultLineColor));

        lineWidth = a.getDimension(R.styleable.GlyphView_lineWidth, lineWidth);
        pointerRadius = a.getDimension(R.styleable.GlyphView_pointerRadius, pointerRadius);

        showGlyph = a.getBoolean(R.styleable.GlyphView_showGlyph, showGlyph);
        inputMode = a.getBoolean(R.styleable.GlyphView_inputMode, inputMode);

        paintGlyph = new Paint();
        paintGlyph.setStrokeWidth(lineWidth);
        paintGlyph.setFlags(Paint.ANTI_ALIAS_FLAG);
        paintGlyph.setColor(lineColor);
        paintGlyph.setStyle(Paint.Style.STROKE);

        paintInput = new Paint();
        paintInput.setStrokeWidth(lineWidth);
        paintInput.setFlags(Paint.ANTI_ALIAS_FLAG);
        paintInput.setColor(inputColor);
        paintInput.setStyle(Paint.Style.STROKE);

        a.recycle();

        if (inputMode) {
            setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            startDrawing(event.getX(0), event.getY(0));
                            return true;
                        case MotionEvent.ACTION_UP:
                            stopDrawing(event.getX(0), event.getY(0));
                            return true;
                        case MotionEvent.ACTION_MOVE:
                            moveDrawing(event.getX(0), event.getY(0));
                            return true;
                    }
                    return false;
                }
            });
        }

    }

    private void startDrawing(float x, float y) {
        if( inputListener != null ) {
            inputListener.startDrawing();
        }
        inputSequence.clear();
        drawing = true;
        moveDrawing(x, y);
    }

    private void stopDrawing(float x, float y) {
        drawing = false;
        moveDrawing(x, y);
        if( inputListener != null ) {
            inputListener.stopDrawing(inputSequence);
        }
    }

    private void moveDrawing(float x, float y) {
        Optional<Integer> pointNear = Glyph.getPointForXY(x - contentWidth / 2, y - contentHeight / 2, glyphSize, glyphSize / 20);
        if (pointNear.isPresent()) {
            if (inputSequence.size() > 0 && inputSequence.get(inputSequence.size() - 1) == pointNear.get()) {
                // already at top, ignore
            } else {
                inputSequence.add(pointNear.get());
            }
        }
        pointerX = x;
        pointerY = y;
        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (showGlyph || inputMode) {
            // Draw dots
            for (int c = 0; c < Glyph.POINTS; c++) {
                final PointF point = glyph.getPoint(c, glyphSize);
                point.offset(contentWidth / 2, contentHeight / 2);
                canvas.drawCircle(point.x, point.y, pointRadius, paintGlyph);
            }

        }

        if (showGlyph) {
            // Draw glyph
            drawGlyph(glyphSequence, paintGlyph, canvas, false);
        }

        if (inputMode && drawing) {
            drawGlyph(inputSequence, paintInput, canvas, true);

        }


    }

    private void drawGlyph(List<Integer> sequence, Paint paint, Canvas canvas, boolean showPointer) {
        if (sequence.size() >= 1) {
            PointF start = Glyph.getPoint(sequence.get(0), glyphSize);
            start.offset(contentWidth / 2, contentHeight / 2);
            for (int c = 1; c < sequence.size(); c++) {
                PointF stop = Glyph.getPoint(sequence.get(c), glyphSize);

                stop.offset(contentWidth / 2, contentHeight / 2);
                canvas.drawLine(start.x, start.y, stop.x, stop.y, paint);

                start = stop;
            }

            if (showPointer && drawing) {
                canvas.drawLine(start.x, start.y, pointerX, pointerY, paint);
            }
        }

        if (showPointer && drawing) {
            canvas.drawCircle(pointerX, pointerY, pointerRadius, paint);
        }

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        contentWidth = getWidth() - paddingLeft - paddingRight;
        contentHeight = getHeight() - paddingTop - paddingBottom;
        glyphSize = Math.min(contentHeight, contentWidth);
        pointRadius = (float) (0.06 / 2 * glyphSize);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int originalWidth = MeasureSpec.getSize(widthMeasureSpec);
        int originalHeight = MeasureSpec.getSize(heightMeasureSpec);

        int calculatedHeight = (int) (originalWidth * aspectRatio);
        int calculatedWidth = (int) (originalHeight / aspectRatio);

        if (calculatedHeight > originalHeight) {
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec(calculatedWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(originalHeight, MeasureSpec.EXACTLY));

        } else {
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec(originalWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(calculatedHeight, MeasureSpec.EXACTLY));

        }

    }

    public Glyph getGlyph() {
        return glyph;
    }

    public void setGlyph(Glyph glyph) {
        this.glyph = glyph;

        glyphSequence.clear();
        for (int i = 0; i < glyph.size(); i++) {
            glyphSequence.add(glyph.getIndex(i));
        }

        postInvalidate();
    }

    public boolean isShowGlyph() {
        return showGlyph;
    }

    public void setShowGlyph(boolean showGlyph) {
        this.showGlyph = showGlyph;
    }

    public void setInputListener(InputListener inputListener) {
        this.inputListener = inputListener;
    }

    public interface InputListener {

        void startDrawing();

        void stopDrawing(List<Integer> inputSequence);
    }
}
