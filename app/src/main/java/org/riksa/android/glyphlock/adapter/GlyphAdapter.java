/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.GlyphList;
import org.riksa.android.glyphlock.ui.GlyphView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by riksa on 14/08/15.
 */
public class GlyphAdapter extends RecyclerView.Adapter<GlyphAdapter.ViewHolder> {

    private final List<Glyph> glyphList;
    private OnGlyphClickListener onGlyphClickListener;

    public GlyphAdapter(GlyphList glyphList) {
        this.glyphList = glyphList.getGlyphs();
        Timber.d("glyphList %s", glyphList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.labeled_glyph, viewGroup, false);
        final ViewHolder vh = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onGlyphClickListener != null) {
                    onGlyphClickListener.onGlyphClicked(vh.glyph);
                }

            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final Glyph glyph = glyphList.get(i);

        viewHolder.glyph = glyph;
        viewHolder.glyphView.setGlyph(glyph);
        viewHolder.textView.setText(glyph.getTitle());

    }

    @Override
    public int getItemCount() {
        return glyphList.size();
    }

    public void setOnGlyphClickListener(OnGlyphClickListener onGlyphClickListener) {
        this.onGlyphClickListener = onGlyphClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.glyph_name)
        TextView textView;

        @Bind(R.id.glyph_view)
        GlyphView glyphView;

        Glyph glyph;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            glyphView.setShowGlyph(true);
        }
    }

    public interface OnGlyphClickListener {
        void onGlyphClicked(Glyph glyph);
    }
}
