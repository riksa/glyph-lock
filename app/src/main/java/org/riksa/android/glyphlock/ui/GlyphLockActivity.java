/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import org.riksa.android.glyphlock.GlyphApplication;
import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.SequenceProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Lazy;
import timber.log.Timber;

public class GlyphLockActivity extends DaggerActivity implements GlyphView.InputListener {
    private static final String KEY_SEQUENCE = "sequence";

    @Bind(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;

    @Bind({R.id.glyph_1, R.id.glyph_2, R.id.glyph_3, R.id.glyph_4, R.id.glyph_5})
    List<ViewGroup> sequenceViews;

    @Bind(R.id.glyph_input)
    GlyphView glyphView;

    @Inject
    Lazy<SequenceProvider> sequenceProvider;

    /**
     * if sequence fail, delay showing new sequence for a while to show the failed glyph
     */
    private static final long NEW_SEQUENCE_DELAY_MILLIS = 500;

    private final List<Glyph> sequence = new ArrayList<>(5);
    private int sequencePointer;
    private boolean sequenceFailed = false;
    private Handler handler;
    private boolean showGlyphs;
    private boolean showSequence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        super.onCreate(savedInstanceState);

        handler = new Handler();

        // TODO: maybe use this to disable status bar
//        http://stackoverflow.com/questions/25284233/prevent-status-bar-for-appearing-android-modified?answertab=active#tab-top
        setContentView(R.layout.activity_glyph_lock);

        getApplicationComponent().inject(this);

        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            setSequence((List<Glyph>) savedInstanceState.getSerializable(KEY_SEQUENCE));
        }
        setSequence(sequenceProvider.get().getSequence());

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(KEY_SEQUENCE, (Serializable) sequence);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        glyphView.setInputListener(this);
        showGlyphs = GlyphPreferenceFragment.isShowGlyph(this);
        showSequence = GlyphPreferenceFragment.isShowSequence(this) || !GlyphPreferenceFragment.isCustomSequenceEnabled(this);

        showSequence();
    }

    @Override
    protected void onStop() {
        if (glyphView != null) {
            glyphView.setInputListener(null);
        }
        super.onStop();
    }

    private GlyphApplication getGlyphApplication() {
        return (GlyphApplication) getApplication();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.d("onNewIntent %s", intent);
    }


    @Override
    protected void onDestroy() {
        if (glyphView != null) {
            glyphView.setInputListener(null);
        }
        super.onDestroy();
    }

    private void showSequence() {
        if (sequence == null) {
            return;
        }

        if (showSequence) {
            GlyphSequenceFragment glyphSequenceFragment = GlyphSequenceFragment.newInstance(sequence);
            glyphSequenceFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.sequence_dialog);
            glyphSequenceFragment.show(getSupportFragmentManager(), GlyphSequenceFragment.TAG);
        }

        ButterKnife.apply(sequenceViews, new ButterKnife.Action<ViewGroup>() {
            @Override
            public void apply(ViewGroup view, int index) {
                view.setVisibility(View.GONE);
            }
        });

        boolean showGlyph = GlyphPreferenceFragment.isShowGlyph(this);
        boolean showGlyphTitle = GlyphPreferenceFragment.isShowGlyphTitle(this);

        for (int i = 0; i < sequence.size(); i++) {
            Glyph glyph = sequence.get(i);
            View view = sequenceViews.get(i);

            GlyphView glyphView = ButterKnife.findById(view, R.id.glyph_view);
            glyphView.setGlyph(glyph);
            glyphView.setShowGlyph(showGlyph && showSequence);

            ImageView glyphViewBackground = ButterKnife.findById(view, R.id.glyph_background);
            glyphViewBackground.setImageLevel(0);

            TextView textView = ButterKnife.findById(view, R.id.glyph_name);
            textView.setVisibility(showGlyphTitle && showSequence ? View.VISIBLE : View.GONE);
            textView.setText(glyph.getPrimaryName());

            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (GlyphPreferenceFragment.isBackUnlockEnabled(this)) {
            Snackbar.make(coordinatorLayout, R.string.snack_unlock_title, Snackbar.LENGTH_SHORT)
                    .setAction(R.string.snack_unlock_action, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            overrideLock();
                        }
                    })
                    .show();
        }
//        super.onBackPressed();
    }

    private void overrideLock() {
        if (GlyphPreferenceFragment.isOverridePasswordRequired(this)) {

            final View passwordView = getLayoutInflater().inflate(R.layout.override_lock_password, null);
            final TextView textView = ButterKnife.findById(passwordView, R.id.password);

            final Context context = getApplicationContext();

            new AlertDialog.Builder(this)
                    .setTitle(R.string.override_lock_title)
                    .setView(passwordView)
                    .setIcon(R.drawable.ic_lock_open)
                    .setPositiveButton(R.string.override_lock_positive, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final CharSequence password = textView.getText();
                            Timber.d("Trying to unlock with password %s", password);
                            if (GlyphPreferenceFragment.overridePasswordMatches(context, password)) {
                                Timber.d("Password matches");
                                unlockDevice();
                            } else {
                                Snackbar.make(coordinatorLayout, R.string.override_lock_wrong_password_snack, Snackbar.LENGTH_SHORT)
                                        .show();
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        } else {
            unlockDevice();
        }

    }

    private void debugUnlock() {
        Timber.d("onClickUnlock");

        unlockDevice();
    }

    private void unlockDevice() {
        setResult(42);
        finish();
    }


    @Override
    public void startDrawing() {
        Timber.d("started Drawing");
    }

    @Override
    public void stopDrawing(List<Integer> inputSequence) {
        Timber.d("Glyph finished %s", inputSequence);

        GlyphView glyphView = null;
        ImageView glyphViewBackground = null;
        if (sequenceViews.size() > sequencePointer) {
            View view = sequenceViews.get(sequencePointer);
            glyphView = ButterKnife.findById(view, R.id.glyph_view);
            glyphViewBackground = ButterKnife.findById(view, R.id.glyph_background);
        }

        final Glyph correctGlyph = sequence.get(sequencePointer);
        if (correctGlyph.matches(inputSequence)) {
            if (glyphView != null) {
                glyphView.setShowGlyph(showGlyphs && showSequence);
            }
            if (glyphViewBackground != null) {
                // if showSequence is disabled, just show green background to not give any hints
                glyphViewBackground.setImageLevel(showSequence ? 1 : 1);
            }

        } else {
            sequenceFailed = true;
            if (glyphView != null) {
                glyphView.setShowGlyph(showGlyphs && showSequence);
            }
            if (glyphViewBackground != null) {
                // if showSequence is disabled, just show green background to not give any hints
                glyphViewBackground.setImageLevel(showSequence ? 2 : 1);
            }
        }


        sequencePointer++;
        if (sequencePointer >= sequence.size()) {
            // finished sequence
            if (sequenceFailed) {
                final int length = GlyphPreferenceFragment.getSelectedSequenceSize(this);
                setSequence(sequenceProvider.get().getSequence());

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showSequence();
                    }
                }, NEW_SEQUENCE_DELAY_MILLIS);
            } else {
                unlockDevice();
            }

        }


    }

    public void setSequence(List<Glyph> newSequence) {
        synchronized (sequence) {
            Timber.d("Set sequence %s", sequence);
            this.sequence.clear();
            this.sequence.addAll(newSequence);
            sequenceFailed = false;
            sequencePointer = 0;
        }
    }
}
