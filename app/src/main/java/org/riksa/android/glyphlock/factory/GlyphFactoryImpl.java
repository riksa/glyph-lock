/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.factory;

import org.riksa.android.glyphlock.model.Glyph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by riksa on 29/08/15.
 */
public class GlyphFactoryImpl implements GlyphFactory {
    @Override
    public Glyph fromSequence(String name, List<Integer> inputSequence) {
        return fromSequence(Arrays.asList(name), inputSequence);
    }

    @Override
    public Glyph fromSequence(List<String> names, List<Integer> inputSequence) {
        return new Glyph(names, new ArrayList<>(inputSequence));
    }
}
