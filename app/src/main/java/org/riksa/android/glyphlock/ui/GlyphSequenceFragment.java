/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.ui;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.squareup.leakcanary.RefWatcher;

import org.riksa.android.glyphlock.GlyphApplication;
import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.dagger.ApplicationComponent;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.GlyphResolver;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GlyphSequenceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GlyphSequenceFragment extends DialogFragment {
    private static final String ARG_SEQUENCE = "sequence";
    public static final String TAG = "sequence";
    private static final long GLYPH_DELAY_MILLIS = 1500;
    public static final int INITIAL_DELAY_MILLIS = 200;

    private List<Glyph> sequence;

    @Bind(R.id.glyph_name)
    TextView textView;

    @Bind(R.id.glyph_view)
    GlyphView glyphView;

    @Inject
    GlyphResolver glyphResolver;

    int sequencePointer = 0;

    private Handler handler;

    private boolean done = false;
    private Runnable finishRunnable = new Runnable() {
        @Override
        public void run() {
            if(!done) {
                Timber.d("dismiss dialog");
                dismiss();
            }
        }
    };

    private Runnable flipRunnable = new Runnable() {
        @Override
        public void run() {
            if( done ) {
                Timber.d("done");
                return;
            }

            if (sequence != null && sequence.size() > sequencePointer) {
                final Glyph glyph = sequence.get(sequencePointer);
                final String name = glyph.getPrimaryName();
                Timber.d("show glyph %s, %s", name, glyph);

                textView.setText(name);
                glyphView.setGlyph(glyph);

                sequencePointer++;
                handler.postDelayed(this, GLYPH_DELAY_MILLIS);
            } else {
                Timber.d("done with sequence");
                handler.post(finishRunnable);
            }

        }
    };

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sequence Sequnce to show
     * @return A new instance of fragment GlyphSequenceFragment.
     */
    public static GlyphSequenceFragment newInstance(List<Glyph> sequence) {
        GlyphSequenceFragment fragment = new GlyphSequenceFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SEQUENCE, (Serializable) sequence);
        fragment.setArguments(args);
        return fragment;
    }

    public GlyphSequenceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onPause() {
        done = true;
        handler.removeCallbacks(flipRunnable);
        handler.removeCallbacks(finishRunnable);
        dismiss();
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getApplicationComponent().inject(this);
        if (getArguments() != null) {
            sequence = (List<Glyph>) getArguments().getSerializable(ARG_SEQUENCE);
        }
        handler = new Handler();
    }

    @Override
    public void onDestroy() {
        dismiss();

        RefWatcher refWatcher = GlyphApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);

        super.onDestroy();
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((GlyphApplication) getActivity().getApplicationContext()).getApplicationComponent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_glyph_sequence, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        textView.setVisibility(GlyphPreferenceFragment.isShowGlyphTitle(getActivity()) ? View.VISIBLE : View.GONE);
        glyphView.setShowGlyph(true);

        startSequence();

    }

    private void startSequence() {
        sequencePointer = 0;
        handler.postDelayed(flipRunnable, INITIAL_DELAY_MILLIS);
    }
}
