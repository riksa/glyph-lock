/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by riksa on 14/08/15.
 */
public class Sequence implements Serializable {
    private final List<String> sequence = new ArrayList<>(5);
    private List<Glyph> glyphs = new ArrayList<>(5);

    public List<String> getSequence() {
        return sequence;
    }

    public List<Glyph> getGlyphs() {
        return glyphs;
    }

    public void add(String name, Glyph glyph) {
        sequence.add(name);
        glyphs.add(glyph);
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "sequence=" + sequence +
                ", glyphs=" + glyphs +
                '}';
    }
}
