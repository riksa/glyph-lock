/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.glyphlock.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Optional;

import org.riksa.android.glyphlock.R;
import org.riksa.android.glyphlock.adapter.GlyphAdapter;
import org.riksa.android.glyphlock.factory.GlyphFactory;
import org.riksa.android.glyphlock.model.Glyph;
import org.riksa.android.glyphlock.model.GlyphList;
import org.riksa.android.glyphlock.model.GlyphResolver;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class DefineUnlockSequenceActivity extends DaggerActivity implements GlyphAdapter.OnGlyphClickListener, GlyphView.InputListener {

    public static final String EXTRA_MODE = "extra_mode";
    public static final int MODE_DEFINE = 1;
    public static final int MODE_VERIFY = 2;

    @Bind(R.id.coordinator)
    protected View coordinator;

    @Bind(R.id.toolbar)
    protected Toolbar toolbar;

    @Inject
    protected GlyphList glyphList;

    @Nullable
    @Bind(R.id.recycler_view)
    protected RecyclerView recyclerView;

    @Bind(R.id.glyph_input)
    protected GlyphView glyphView;

    @Bind(R.id.fab_ok)
    protected View fabOk;

    @Inject
    protected GlyphFactory glyphFactory;

    @Bind({R.id.glyph_1, R.id.glyph_2, R.id.glyph_3, R.id.glyph_4, R.id.glyph_5})
    protected List<ViewGroup> sequenceViews;

    @Inject
    protected GlyphResolver glyphResolver;

    private RecyclerView.LayoutManager layoutManager;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private GlyphAdapter adapter;

    private final List<Glyph> sequence = new ArrayList<>(5);
    public static final String EXTRA_SEQUENCE = "extra_sequence";
    private int customCounter = 0;
    private int mode = MODE_DEFINE;

    private List<Glyph> unlockSequence;
    private boolean matches = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);
        setContentView(R.layout.activity_define_unlock_sequence);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            layoutManager = new GridLayoutManager(this, getResources().getInteger(R.integer.glyph_span_count));
            recyclerView.setLayoutManager(layoutManager);

            adapter = new GlyphAdapter(glyphList);
            adapter.setOnGlyphClickListener(this);
            recyclerView.setAdapter(adapter);
        }

        if( getIntent() != null && getIntent().getExtras() != null ) {
            mode = getIntent().getIntExtra(EXTRA_MODE, MODE_DEFINE);
            unlockSequence = (List<Glyph>) getIntent().getSerializableExtra(EXTRA_SEQUENCE);
        }

        updateSequence();

    }

    @Override
    protected void onStart() {
        super.onStart();
        updateSequence();
        glyphView.setInputListener(this);
    }

    @Override
    protected void onStop() {
        if (glyphView != null) {
            glyphView.setInputListener(null);
        }
        super.onStop();
    }

    private void updateSequence() {
        final int selectedSequenceSize = GlyphPreferenceFragment.getSelectedSequenceSize(this);

        ButterKnife.apply(sequenceViews, new ButterKnife.Action<ViewGroup>() {
            @Override
            public void apply(ViewGroup view, int index) {
//                view.setVisibility(View.VISIBLE);
                view.setVisibility(index < selectedSequenceSize ? View.VISIBLE : View.GONE);
            }
        });

        for (int i = 0; i < selectedSequenceSize; i++) {
            final View view = sequenceViews.get(i);
            GlyphView glyphView = ButterKnife.findById(view, R.id.glyph_view);
            TextView textView = ButterKnife.findById(view, R.id.glyph_name);
            ImageView imageView = ButterKnife.findById(view, R.id.glyph_background);

            if (sequence.size() > i) {
                // we have a glyph
                Glyph glyph = sequence.get(i);
                glyphView.setShowGlyph(true);
                glyphView.setGlyph(glyph);

                textView.setText(glyph.getTitle());

                int level = 1;
                if( mode == MODE_VERIFY ) {
                    if( !glyph.matches(unlockSequence.get(i).getIndices())) {
                        // show red if input did not verify as correct glyph
                        matches = false;
                        level = 2;
                    }
                }
                imageView.setImageLevel(level);


                } else {
                glyphView.setShowGlyph(false);
                glyphView.setGlyph(Glyph.EMPTY);

                textView.setText(null);

                imageView.setImageLevel(2);

            }

        }

    }

    @OnClick(R.id.fab_ok)
    public void onClickOk() {
        Snackbar.make(coordinator, R.string.snack_confirm_unlock_title, Snackbar.LENGTH_LONG)
                .setAction(R.string.snack_confirm_unlock_action, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent data = new Intent();
                        data.putExtra(EXTRA_SEQUENCE, (Serializable) sequence);
                        setResult(RESULT_OK, data);
                        finish();
                    }
                })
                .show();

    }

    @Override
    public void onGlyphClicked(Glyph glyph) {
        resetSequenceIfNeeded();

        sequence.add(glyph);
        updateSequence();

        checkSequenceCompletion();

    }

    private void resetSequenceIfNeeded() {
        if (sequence.size() >= GlyphPreferenceFragment.getSelectedSequenceSize(this)) {
            sequence.clear();
            customCounter = 0;
            matches = true;
        }
    }

    private void checkSequenceCompletion() {
        if (sequence.size() == GlyphPreferenceFragment.getSelectedSequenceSize(this)) {
            if( mode == MODE_VERIFY && !matches) {
                fabOk.setVisibility(View.GONE);
            } else {
                fabOk.setVisibility(View.VISIBLE);
            }
        } else {
            fabOk.setVisibility(View.GONE);
        }
    }

    @Override
    public void startDrawing() {
        Timber.d("start drawing");

    }

    @Override
    public void stopDrawing(List<Integer> inputSequence) {
        Timber.d("Glyph finished %s", inputSequence);

        resetSequenceIfNeeded();

        final Optional<Glyph> glyphOptional = glyphResolver.detectGlyph(inputSequence);
        if (glyphOptional.isPresent()) {
            sequence.add(glyphOptional.get());
        } else {
            // custom glyph
            final String name = getString(R.string.custom_glyph, ++customCounter);
            Glyph glyph = glyphFactory.fromSequence(name, inputSequence);
            Timber.d("Custom glyph %s", glyph);
            sequence.add(glyph);
        }

        updateSequence();
        checkSequenceCompletion();

    }
}
