# Glyph Lock #

Work in progress, Ingress Glyph based lock screen for Android devices. The idea is to learn glyphs
more easily. My idea so far is for the lock screen to operate in user selectable modes:
 * Easy mode: Lockscreen shows you the glyph(s) and you need to draw them correctly to unlock. If
   you fail to draw a glyph correctly, you can try again. Button to replay the sequence.
   If you fail to draw the correct glyph, the solution will be shown to you above the drawing area.
 * Hard mode: Lockscreen shows you the glyph(s) and you need to draw them correctly to unlock. If
   you fail to draw the sequence correctly, you will get a new sequence.
 * Lock mode: You choose the glyphs required to open the device, no hints

First two are just to learn glyphs and keep screen locked, last one is sort of replacement for real
lock screen.


### What needs to be done ###
 * Define all the glyphs in res/raw/glyphs.json
 * Define all the sequences in res/raw/sequences.json
 * GlyphView to draw a glyph
 * Fragment to prompt the user for a glyph sequence
 * Launcher replacement that the user sets up as the default launcher, the launcher is the actual
   lockscreen, and upon "unlocking" we launch the actual launcher the user wants to use. This is to
   work around the Home button otherwise bypassing the lock screen.


### How do I get set up? ###

* clone the repo, checkout the "develop" branch and './gradlew clean installDebug'

### Who do I talk to? ###

* riku salkia <riksa@iki.fi> RikiSorsanVeli in Ingress
